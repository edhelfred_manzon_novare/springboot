package hk.com.novare.sample.UserService;

import hk.com.novare.sample.Repository.UserRepo;
import hk.com.novare.sample.models.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("userService")
public class UserServiceImpl implements UserService {
    @Autowired
    UserRepo userRepo;

    public void sendEmail(UserModel userModel) {
        userRepo.sendEmail(userModel);
    }
}
