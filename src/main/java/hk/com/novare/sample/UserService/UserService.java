package hk.com.novare.sample.UserService;

import hk.com.novare.sample.models.UserModel;


public interface UserService {
    void sendEmail(UserModel userModel);
}
