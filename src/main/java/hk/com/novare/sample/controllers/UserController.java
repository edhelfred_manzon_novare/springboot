package hk.com.novare.sample.controllers;

import hk.com.novare.sample.UserService.UserService;
import hk.com.novare.sample.models.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("")
public class UserController {


    @Autowired
    private UserService userService;


    @PostMapping("/email")
    public UserModel userDetail(@RequestBody UserModel userModel){
        userService.sendEmail(userModel);
        return userModel;
    }

}
