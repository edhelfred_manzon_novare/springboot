package hk.com.novare.sample.Repository;

import hk.com.novare.sample.models.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Repository;


@Repository("userRepo")
public class UserRepoImpl implements UserRepo {

    @Autowired
    public JavaMailSender emailSender;

    public void sendEmail(UserModel userModel){
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setTo(userModel.getEmail());
        simpleMailMessage.setSubject(userModel.getSubject());
        simpleMailMessage.setText(userModel.getBody());
        emailSender.send(simpleMailMessage);
    }

}
