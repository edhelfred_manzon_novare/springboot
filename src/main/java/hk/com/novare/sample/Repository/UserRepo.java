package hk.com.novare.sample.Repository;

import hk.com.novare.sample.models.UserModel;


public interface UserRepo {
    void sendEmail(UserModel userModel);
}
